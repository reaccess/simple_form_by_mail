# php-simpleMailForm

Module PHP autonome de **gestion de formulaires** avec envoi mail.

----------


## Installation

*  Copier le dossier `formulaire` dans le répertoire où se situe la page web,
*  Attribuer les droits en écriture (777) au sous-dossier `formulaire/logs`,
*  Éditer le fichier `formulaire/config.ini.php` pour configurer le formulaire,
*  Dans les pages souhaitées du site, inclure la balise suivante, en adaptant simplement le nom du dossier "formulaire" :

> `<?php`

> `$dossierFormulaire = "formulaire";`

> `include( $dossierFormulaire . "/bin/form.php" );`

> `?>`


----------


## Configuration

### Liste des champs
Il est possible de créer autant de champs qu'on veut.
Pour déclarer les champs :
*  1 ligne par champ
*  `[name]=[label];[obligatoire];[type];[classes css]`

Types possibles : `text` / `tel` / `email` / `textarea` / `submit`

### Destinataires
Autant de destinataires qu'on veut, déclarés sur une seule ligne, séparés par des points virgules `;`

### Copie
La personne qui emet la demande doit-elle recevoir une copie du mail ?

### Archivage
Créer et garder une archive .html de la demande dans le dossier "logs" ?

### Traductions
Textes et traductions

### Configuration de l'envoi des mails
Paramètres relatifs à l'envoi du mail

----------


## Fonctionnalités

*  Liste des champs du formulaire paramétrable,
*  Envoi par mail à 1 ou plusieurs destinataires,
*  Messages, emails personnalisables,
*  Multilingue,
*  Copie optionnelle du mail à l'internaute,
*  Archivage optionnel.

----------


## Trucs et astuces

Il est possible de renommer le dossier `formulaire`, et/ou de le dupliquer afin d'utiliser plusieurs formulaires sur un même site.

Pour modifier un formulaire existant, il faut modifier le fichier de configuration à la main.

----------



*php-simpleMailForm - Version 5.0.7 - Copyright &copy; 2016-2028 Natural-net / Reacccess*

