<?php
if (session_status() == PHP_SESSION_NONE) session_start();
if ( !class_exists('Formulaire') ) require_once( "formulaire.class.php" );
if ( !isset($dossierFormulaire) ) $nomDossierFormulaire = ( isset($_POST['nomDossierFormulaire']) && $_POST['nomDossierFormulaire'] != '' ) ? $_POST['nomDossierFormulaire'] : '';
else $nomDossierFormulaire = $dossierFormulaire;
new Formulaire( $nomDossierFormulaire );
?>
