<?php
/**
 * Contient la classe Formulaire
 */
class Formulaire {

  private $typesAcceptes;
  private $nomFormulaire;
  private $nomDossierFormulaire;
  private $nomDossierFormulaireCalcule;
  private $action_form;
  private $page_form;
  private $mailToSend;
  private $config;


  /*
   * Entrée principale
   * Traitements des données soumises via formulaire
   * Affichage du formulaire
   */
  public function __construct( $nomDossierFormulaire = false ) {
    global $_POST;
    $this->config_verif( $nomDossierFormulaire );  // Vérifications des prérequis
    $this->config_init();                          // Initialisations
    if ( $this->action_form == "envoyer" ) {
      $mail = $this->email_prepare_envoi();        // Préparation du mail
    }
    $this->affiche_formulaire();                   // Affichage du formulaire
    $this->mise_a_jour_session();                  // Mise à jour de la session
  }


  /*
   * Vérifications des prérequis pour le bon fonctionnement
   */
  private function config_verif( $nomDossierFormulaire = false ) {
    $error = false;
    $errorMessage = array();
    $this->nomDossierFormulaire = ( $nomDossierFormulaire )        ? $nomDossierFormulaire : '';
    $this->nomFormulaire = ( $this->nomDossierFormulaire=='' && isset($_POST['dossier_form']) && $_POST['dossier_form']!='' ) ? $_POST['dossier_form'] : $nomDossierFormulaire;
    $this->nomDossierFormulaireCalcule = ( $this->nomDossierFormulaire=='' && isset($_POST['dossier_form']) && $_POST['dossier_form']!='' ) ? '../../' . $_POST['dossier_form'] : $nomDossierFormulaire;
    $this->config               = array(
                                         'Champs' => array(),
                                         'Traductions' => array(),
                                         'Email' => array(),
                                         'Configuration' => array(),
                                         'Archivage' => array(),
                                         'Copie' => array(),
                                         'Securite' => array()
                                        );
    /*
     * Présence des fichiers et autorisations
     */
    $fichierConfig  = ( $this->nomDossierFormulaireCalcule != '' ) ? $this->nomDossierFormulaireCalcule . "/config.ini.php" : "../config.ini.php";
    if ( !file_exists($fichierConfig) || !file($fichierConfig) ) {
      $error = true;
      $errorMessage[] = 'Fichier manquant : &laquo;' . $fichierConfig . '&raquo;';
    }
    /*
     * Valeurs de configuration
     */
    $this->formulaire_get_config(); // Récupération du fichier de configuration config.ini.php
    if ( !isset($this->config['Champs']) ) {
      $error = true;
      $errorMessage[] = 'Groupe manquant : &laquo;Champs&raquo;';
    }
    elseif ( sizeof($this->config['Champs'])<1 ) {
      $error = true;
      $errorMessage[] = 'Groupe vide : &laquo;Champs&raquo; (au moins 1 champs est requis)';
    }
    if ( !isset($this->config['Email']) ) {
      $error = true;
      $errorMessage[] = 'Groupe manquant : &laquo;Email&raquo;';
    }
    elseif ( sizeof($this->config['Email'])<1 ) {
      $error = true;
      $errorMessage[] = 'Groupe vide : &laquo;Email&raquo;';
    }
    else {
      if ( !isset($this->config['Email']['destinataires']) || trim($this->config['Email']['destinataires'])=='' ) {
        $error = true;
        $errorMessage[] = 'Groupe &laquo;Email&raquo; : configuration manquante : destinataires (au moins 1 destinataire est requis)';
      }
    }
    if ( !isset($this->config['Securite']) ) {
      $error = true;
      $errorMessage[] = 'Groupe manquant : &laquo;Securite&raquo;';
    }
    elseif ( sizeof($this->config['Securite'])<1 ) {
      $error = true;
      $errorMessage[] = 'Groupe vide : &laquo;Securite&raquo; (au moins 1 champs est requis)';
    }
    else {
      if ( !isset($this->config['Securite']['nom_de_domaine']) || trim($this->config['Securite']['nom_de_domaine'])=='' ) {
        $error = true;
        $errorMessage[] = 'Groupe &laquo;Securite&raquo; : configuration manquante : nom_de_domaine (nom de domaine www.exemple.com obligatoire)';
      }
    }
    // Redirection erreur de configuration
    if ( $error ) {
      echo '<div class="alerte">';
      echo '<h3>Erreur d’installation ou de configuration</h3>';
      foreach ( $errorMessage as $key => $erreur ) echo '<p>' . $erreur . '</p>';
      echo '</div>';
      exit;
    }
  }


  /*
   * Récupération de la configuration
   */
  private function config_init() {
    global $_POST;
    $this->action_form          = ( isset($_POST['action_form']) ) ? $_POST['action_form'] : '';
    $this->page_form            = ( isset($_POST['page_form']) )   ? $_POST['page_form']   : '';
    $this->typesAcceptes        = array('text', 'tel', 'email', 'textarea', 'submit');
    $this->mailToSend           = array();
  }


  /*
   * Récupération de la configuration
   */
  private function formulaire_get_config() {
    $fichierConfig  = ( $this->nomDossierFormulaireCalcule != '' ) ? $this->nomDossierFormulaireCalcule . "/" : "../";
    $fichierConfig .= 'config.ini.php';
    if ( file_exists($fichierConfig) && $fichier_lecture = file($fichierConfig) ) {
      // récupération de la config
      foreach ( $fichier_lecture as $ligne ) {
        $ligne_propre = trim($ligne);
        if ( preg_match("#^\[(.+)\]$#", $ligne_propre, $matches) ) $groupe = $matches[1];
        else {
          if ( isset($ligne_propre) && isset($ligne_propre[0]) && $ligne_propre[0] != ';' ) {
            $ligneArray = explode("=", $ligne, 2);
            $cle        = $ligneArray[0];
            $valeur     = ( isset($ligneArray[1]) ) ? $ligneArray[1] : false ;
            if ( $valeur && isset($this->config[$groupe]) ) $this->config[$groupe][$cle] = trim($valeur);
          }
        }
      }
    }
  }

  /*
   * Contrôles de sécurité
   */
  private function formulaire_reception_donnees_post() {
    foreach ( $_POST as $key => $value ) { if ( $key != 'action_form' ) { $$key = $_POST[$key]; $_SESSION[$this->nomFormulaire . '_' . $key] = htmlentities($$key); } }
  }

  /*
   * Contrôles de sécurité
   */
  private function formulaire_controle_securite() {
    $error      = false;
    $urlPage = ( $this->page_form != '' && isset($this->config['Securite']['nom_de_domaine']) && $this->config['Securite']['nom_de_domaine'] != '' ) ? '//' . $this->config['Securite']['nom_de_domaine'] . $_SESSION[$this->nomFormulaire . '_' . 'page_form'] : false;
    if ( $urlPage && $_SERVER['HTTP_REFERER'] != 'http:' . $urlPage && $_SERVER['HTTP_REFERER'] != 'https:' . $urlPage ) $error = true; // Referer
    if ( !isset($_SESSION[$this->nomFormulaire . '_' . '_vsId']) || !isset($_POST['_vsId']) || $_SESSION[$this->nomFormulaire . '_' . '_vsId'] !== $_POST['_vsId'] ) $error = true; // ID unique
    // Redirection erreur de sécurité
    if ( $error ) {
      $_SESSION[$this->nomFormulaire . '_' . 'erreurTechnique'] = ( isset($this->config['Traductions']['erreur_technique_titre']) && $this->config['Traductions']['erreur_technique_titre']!='' ) ? '<h3>' . $this->config['Traductions']['erreur_technique_titre'] . '</h3>' : '<h3>Erreur</h3>';
      header('Location: ' . $this->page_form);
      exit;
    }
  }

  /*
   * Contrôles de saisie
   */
  private function formulaire_controle_saisie() {
    $error      = false;
    $errorInput = array();
    foreach ( $this->config['Champs'] as $name => $paramsString ) {
      $params = explode(";", $paramsString);
      $obligatoire = ( isset($params[1]) && trim($params[1])!='' ) ? trim($params[1]) : false;
      $type = ( isset($params[2]) && in_array(trim($params[2]), $this->typesAcceptes) ) ? trim($params[2]) : 'text';
      switch ( $type ) {
        case "textarea" :
        case "tel" :
        case "email" :
        case "text" :
          if ( $obligatoire ) {
            if ( strlen($_SESSION[$this->nomFormulaire . '_' . $name]) < 1 ) {
              $error        = true;
              $errorInput[] = $name;
            }
          }
          if ( $type=='email' && !in_array($name, $errorInput) ) {
            if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$_SESSION[$this->nomFormulaire . '_' . $name])) {
              $error        = true;
              $errorInput[] = $name;
            }
          }
          break;
        default :
          break;
      }
    }
    // Redirection formulaire incomplet
    if ( $error ) {
      $_SESSION[$this->nomFormulaire . '_' . 'errorInput'] = $errorInput;
      header('Location: ' . $this->page_form);
      exit;
    }
  }

  /*
   * Préparation des paramètres d'envois
   */
  private function email_initialise_parametres() {
    $this->mailToSend['smtp_hostname']    = ( isset($this->config['Configuration']['smtp_hostname'])    && $this->config['Configuration']['smtp_hostname']!='' )    ? $this->config['Configuration']['smtp_hostname']    : '';
    $this->mailToSend['smtp_port']        = ( isset($this->config['Configuration']['smtp_port'])        && $this->config['Configuration']['smtp_port']!='' )        ? $this->config['Configuration']['smtp_port']        : '587';
    $this->mailToSend['smtp_encryption']  = ( isset($this->config['Configuration']['smtp_encryption'])  && $this->config['Configuration']['smtp_encryption']!='' )  ? $this->config['Configuration']['smtp_encryption']  : 'tls';
    $this->mailToSend['smtp_auth']        = ( isset($this->config['Configuration']['smtp_auth'])        && $this->config['Configuration']['smtp_auth']!='' )        ? $this->config['Configuration']['smtp_auth']        : true;
    $this->mailToSend['smtp_username']    = ( isset($this->config['Configuration']['smtp_username'])    && $this->config['Configuration']['smtp_username']!='' )    ? $this->config['Configuration']['smtp_username']    : '';
    $this->mailToSend['smtp_password']    = ( isset($this->config['Configuration']['smtp_password'])    && $this->config['Configuration']['smtp_password']!='' )    ? $this->config['Configuration']['smtp_password']    : '';
    $this->mailToSend['smtp_from_email']  = ( isset($this->config['Configuration']['smtp_from_email'])  && $this->config['Configuration']['smtp_from_email']!='' )  ? $this->config['Configuration']['smtp_from_email']  : $this->mailToSend['smtp_username'];
    $this->mailToSend['smtp_from_name']   = ( isset($this->config['Configuration']['smtp_from_name'])   && $this->config['Configuration']['smtp_from_name']!='' )   ? $this->config['Configuration']['smtp_from_name']   : $this->mailToSend['smtp_username'];
    $this->mailToSend['smtp_reply_email'] = ( isset($this->config['Configuration']['smtp_reply_email']) && $this->config['Configuration']['smtp_reply_email']!='' ) ? $this->config['Configuration']['smtp_reply_email'] : $this->mailToSend['smtp_from_email'];
    $this->mailToSend['smtp_reply_name']  = ( isset($this->config['Configuration']['smtp_reply_name'])  && $this->config['Configuration']['smtp_reply_name']!='' )  ? $this->config['Configuration']['smtp_reply_name']  : $this->mailToSend['smtp_from_name'];
    $this->mailToSend['mail_subject']     = ( isset($this->config['Traductions']['mail_subject'])       && $this->config['Traductions']['mail_subject']!='' )       ? $this->config['Traductions']['mail_subject']       : 'Contact';
  }

  /*
   * Construction du corps du mail
   */
  private function email_construit_message() {
    $this->mailToSend['mail_destinataires'] = ( isset($this->config['Email']['destinataires']) && $this->config['Email']['destinataires']!='' ) ? explode(';', $this->config['Email']['destinataires']) : array();
    $this->mailToSend['mail_body_text']     = '';
    $this->mailToSend['content_html']       = '';
    $this->mailToSend['mail_body_text']    .= ( isset($this->config['Traductions']['mail_intro']) && $this->config['Traductions']['mail_intro']!='' ) ? $this->config['Traductions']['mail_intro'] . "\r\n" . "\r\n" : '';
    $this->mailToSend['content_html']      .= ( isset($this->config['Traductions']['mail_intro']) && $this->config['Traductions']['mail_intro']!='' ) ? '<p>' . $this->config['Traductions']['mail_intro'] . '</p><hr>': '';
    $this->mailToSend['content_html']      .= '<table>';
    foreach ( $this->config['Champs'] as $name => $paramsString ) {
      $params = explode( ";", $paramsString );
      $label = ( isset( $params[0] ) && trim( $params[0] )!='' ) ? trim( $params[0] ) : '';
      $type = ( isset($params[2]) && in_array(trim($params[2]), $this->typesAcceptes) ) ? trim($params[2]) : 'text';
      if ( trim($_SESSION[$this->nomFormulaire . '_' . $name])!='' && $type != 'submit' ) {
        $this->mailToSend['mail_body_text'] .= '> ' . $label . ' : ' . $_SESSION[$this->nomFormulaire . '_' . $name] . "\r\n" . "\r\n";
        $this->mailToSend['content_html']   .= '<tr><td>' . $label . '</td><td>';
        $this->mailToSend['content_html']   .= ($type=="textarea") ? nl2br($_SESSION[$this->nomFormulaire . '_' . $name]): $_SESSION[$this->nomFormulaire . '_' . $name];
        $this->mailToSend['content_html']   .= '</td></tr>';
      }
    }
    $this->mailToSend['mail_body_text']   .= ( isset($this->config['Traductions']['mail_signature']) && $this->config['Traductions']['mail_signature']!='' ) ? "\r\n" . "\r\n" . $this->config['Traductions']['mail_signature']: '';
    $this->mailToSend['content_html']     .= '</table>';
    $this->mailToSend['content_html']     .= ( isset($this->config['Traductions']['mail_signature']) && $this->config['Traductions']['mail_signature']!='' ) ? '<hr><p>' . $this->config['Traductions']['mail_signature'] . '</p>': '';
    $this->mailToSend['mail_body_html']    = '';
    if ( isset($this->config['Email']['format_mail']) && $this->config['Email']['format_mail'] == 'html' ) {
      if ( isset($this->config['Email']['template_html']) && $this->config['Email']['template_html']!='' ) {
        /*
         * Chargement dans le template html
         */
        $handle = fopen("../" . $this->config['Email']['template_html'], "r");
        if ($handle) {
          while (!feof($handle)) {
            $buffer = fgets($handle, 4096);
            $this->mailToSend['mail_body_html'] .= ereg_replace('__contenu__', $this->mailToSend['content_html'], $buffer);
          }
          fclose($handle);
        }
      }
      else {
        /*
         * Mail html sans template
         */
        $this->mailToSend['mail_body_html'] = '<html><meta charset="utf-8"><title>' . $this->mailToSend['mail_subject'] . ' [' . date("d/m/Y à H:i") . ']</title></head><body>'
          . '<h1>'
          . $this->mailToSend['mail_subject']
          . '</h1>'
          . $this->mailToSend['content_html']
          . '</body></html>';
      }
    }
  }


  /*
   * Préparation du mail
   */
  private function email_prepare_envoi() {
    $this->formulaire_reception_donnees_post();  // Mise en session des données POST
    $this->formulaire_controle_securite();       // Contrôles de sécurité
    $this->formulaire_controle_saisie();         // Contrôles de saisie
    $this->email_initialise_parametres();        // Préparation des paramètres d'envois
    $this->email_construit_message();            // Construction du corps du mail

    $subject  = $this->mailToSend['mail_subject'];
    $msg      = ( $this->mailToSend['mail_body_html'] != '' ) ? $this->mailToSend['mail_body_html'] : $this->mailToSend['content_html'];
    $headers  = "MIME-Version: 1.0 \n";
    $headers .= "Content-Transfer-Encoding: 8bit \n";
    $headers .= "Content-type: text/html; charset=utf-8 \n";
    $headers .= "From: " . $this->mailToSend['smtp_from_name'] . " <" . $this->mailToSend['smtp_from_email'] . "> \n";
    $headers .= "Reply-To: " . $this->mailToSend['smtp_from_email'] . " \n";

    $error = false;
    foreach ( $this->mailToSend['mail_destinataires'] as $key => $destinataire ) if ( trim($destinataire) != '' ) {
      if ( !mail (trim($destinataire), $subject, $msg, $headers) ) $error = 1;
    }
    /*
     * Copie au demandeur
     */
    if ( !isset($this->config['Copie']['copie_internaute']) || $this->config['Copie']['copie_internaute']!='0' ) {
      $nomChampEmail = ( isset($this->config['Copie']['nom_champ_email']) && $this->config['Copie']['nom_champ_email']!='' ) ? $this->config['Copie']['nom_champ_email'] : 'email';
      if ( isset($_SESSION[$this->nomFormulaire . '_' . $nomChampEmail]) && $_SESSION[$this->nomFormulaire . '_' . $nomChampEmail]!='' ) mail (trim($_SESSION[$this->nomFormulaire . '_' . $nomChampEmail]), $subject, $msg, $headers);
    }
    $this->email_archive();
    if ( $error ) {
      $_SESSION[$this->nomFormulaire . '_' . 'erreurSmtp'] = 1;
      header('Location: ' . $this->page_form); exit;
    } else {                                     // Mail envoyé avec succès
      foreach ( $_SESSION as $key => $value ) unset($_SESSION[$key]);
      $_SESSION[$this->nomFormulaire . '_' . 'successMessage'] = '1'; header('Location: ' . $this->page_form); exit;
    }
  }



  /*
   * Archivage
   */
  private function email_archive() {
    if ( !isset($this->config['Archivage']['logs']) || $this->config['Archivage']['logs']!='0' ) {
      $nomFichierLog = '../logs/' . date("Y-m-d_His") . '.html' ;
      $fichierLog = fopen($nomFichierLog, 'a+');
      if ( $this->mailToSend['mail_body_html'] != '' ) fputs($fichierLog, $this->mailToSend['mail_body_html']);
      else fputs($fichierLog, '<html><meta charset="utf-8"><title>' . $this->mailToSend['mail_subject'] . ' [' . date("d/m/Y à H:i") . ']</title><body><h1>' . $this->mailToSend['mail_subject'] . '</h1>' . $this->mailToSend['content_html'] . '</body></html>');
      fclose($fichierLog);
    }
  }



  /*
   * Messages d'erreur controles de saisies
   */
  private function affiche_message_erreur_saisie() {
    if ( isset($_SESSION[$this->nomFormulaire . '_' . 'errorInput']) ) {
      if ( is_array($_SESSION[$this->nomFormulaire . '_' . 'errorInput']) ) {
        if ( sizeof($_SESSION[$this->nomFormulaire . '_' . 'errorInput']) > 0 ) {
          echo '<div class="alerte">';
          echo ( isset($this->config['Traductions']['erreur_saisie_titre']) && $this->config['Traductions']['erreur_saisie_titre']!='' ) ? '<h3>' . $this->config['Traductions']['erreur_saisie_titre'] . '</h3>' : '';
          echo (  ( isset($this->config['Traductions']['erreur_saisie_soustitre']) && $this->config['Traductions']['erreur_saisie_soustitre']!='' ) ||
                  ( isset($this->config['Traductions']['erreur_saisie_texte']) && $this->config['Traductions']['erreur_saisie_texte']!='' ) ) ? '<p>' : '';
          echo ( isset($this->config['Traductions']['erreur_saisie_soustitre']) && $this->config['Traductions']['erreur_saisie_soustitre']!='' ) ? '<strong>' . $this->config['Traductions']['erreur_saisie_soustitre'] . '</strong>' : '';
          echo ( isset($this->config['Traductions']['erreur_saisie_texte']) && $this->config['Traductions']['erreur_saisie_texte']!='' ) ? $this->config['Traductions']['erreur_saisie_texte'] : '';
          echo (  ( isset($this->config['Traductions']['erreur_saisie_soustitre']) && $this->config['Traductions']['erreur_saisie_soustitre']!='' ) ||
                  ( isset($this->config['Traductions']['erreur_saisie_texte']) && $this->config['Traductions']['erreur_saisie_texte']!='' ) ) ? '</p>' : '';
          echo '</div>';
        }
      }
    }
  }



  /*
   * Messages d'erreur smtp
   * problème de configuration -> cf config.ini.php
   */
  private function affiche_message_smtp() {
    if ( isset( $_SESSION[$this->nomFormulaire . '_' . 'erreurSmtp'] ) ) {
      echo '<div class="alerte">';
      echo ( isset($this->config['Traductions']['erreur_smtp_titre']) && $this->config['Traductions']['erreur_smtp_titre']!='' ) ? '<h3>' . $this->config['Traductions']['erreur_smtp_titre'] . '</h3>' : '';
      echo ( isset($this->config['Traductions']['erreur_smtp_texte']) && $this->config['Traductions']['erreur_smtp_texte']!='' ) ? '<p>' . $this->config['Traductions']['erreur_smtp_texte'] . '</p>' : '';
      echo ( isset($this->config['Traductions']['erreur_smtp']) && $this->config['Traductions']['erreur_smtp']!='' ) ? '<quote>' . $this->config['Traductions']['erreur_smtp'] . '</quote>' : '';
      echo '</div>';
    }
  }



  /*
   * Messages d'erreur technique
   * problème de configuration -> cf config.ini.php
   */
  private function affiche_message_technique() {
    if ( isset( $_SESSION[$this->nomFormulaire . '_' . 'erreurTechnique'] ) ) {
      echo '<div class="alerte">';
      echo ( isset($this->config['Traductions']['erreur_technique_titre']) && $this->config['Traductions']['erreur_technique_titre']!='' ) ? '<h3>' . $this->config['Traductions']['erreur_technique_titre'] . '</h3>' : '';
      echo ( isset($this->config['Traductions']['erreur_technique_texte']) && $this->config['Traductions']['erreur_technique_texte']!='' ) ? '<p>' . $this->config['Traductions']['erreur_technique_texte'] . '</p>' : '';
      echo ( isset($this->config['Traductions']['erreurTechnique']) && $this->config['Traductions']['erreurTechnique']!='' ) ? '<quote>' . $this->config['Traductions']['erreurTechnique'] . '</quote>' : '';
      echo '</div>';
    }
  }



  /*
   * Messages de confirmation d'envoi
   */
  private function affiche_message_success() {
    if ( isset( $_SESSION[$this->nomFormulaire . '_' . 'successMessage'] ) ) {
      echo '<div class="success">';
      echo ( isset($this->config['Traductions']['succes_titre']) && $this->config['Traductions']['succes_titre']!='' ) ? '<h3>' . $this->config['Traductions']['succes_titre'] . '</h3>' : '';
      echo ( isset($this->config['Traductions']['succes_texte']) && $this->config['Traductions']['succes_texte']!='' ) ? '<p>' . $this->config['Traductions']['succes_texte'] . '</p>' : '';
      echo '</div>';
    }
  }



  /*
   * Mise à jour de la session
   */
  private function mise_a_jour_session() {
    if ( isset( $_SESSION[$this->nomFormulaire . '_' . 'errorInput'] ) )       unset($_SESSION[$this->nomFormulaire . '_' . 'errorInput']);
    if ( isset( $_SESSION[$this->nomFormulaire . '_' . 'erreurTechnique'] ) )  unset($_SESSION[$this->nomFormulaire . '_' . 'erreurTechnique']);
    if ( isset( $_SESSION[$this->nomFormulaire . '_' . 'erreurSmtp'] ) )       unset($_SESSION[$this->nomFormulaire . '_' . 'erreurSmtp']);
    if ( isset( $_SESSION[$this->nomFormulaire . '_' . 'successMessage'] ) )   unset($_SESSION[$this->nomFormulaire . '_' . 'successMessage']);
  }



  /*
   * Messages de confirmation d'envoi
   */
  private function affiche_formulaire( ) {
    $_vsId = md5(uniqid(microtime(), true));
    $_SESSION[$this->nomFormulaire . '_' . '_vsId'] = $_vsId;
    $this->affiche_message_erreur_saisie();        // Message erreur controles de saisies
    $this->affiche_message_technique();            // Message erreur technique
    $this->affiche_message_smtp();                 // Message erreur smtp
    $this->affiche_message_success();              // Message réussite envoi formulaire

    echo '<form class="form" method="post" action="' . $this->nomDossierFormulaire . '/bin/form.php">
  ';
    echo '<input name="_vsId" value="' .  $_vsId . '" type="hidden">
  ';
    echo '<input name="dossier_form" value="' . $this->nomDossierFormulaire . '" type="hidden">
  ';
    echo '<input name="page_form" value="' . $_SERVER['PHP_SELF'] . '" type="hidden">
  ';
    echo '<input name="action_form" value="envoyer" type="hidden">
  ';
    $obligatoires = false;
    foreach ( $this->config['Champs'] as $name => $paramsString ) {
      $params = explode( ";", $paramsString );
      $label = ( isset( $params[0] ) && trim( $params[0] )!='' ) ? trim( $params[0] ) : false;
      $obligatoire = ( isset( $params[1] ) && trim( $params[1] )!='' ) ? trim( $params[1] ) : false;
      $type = ( isset( $params[2] ) && in_array( trim( $params[2] ), $this->typesAcceptes ) ) ? trim( $params[2] ) : 'text';
      $css = ( isset( $params[3] ) && trim( $params[3] )!='' ) ? trim( $params[3] ) : false;
      $obligatoires = ( $obligatoire ) ? true : $obligatoires;
      switch ( $type ) {
        case "textarea" :
          echo '<p';
          if ( isset( $_SESSION[$this->nomFormulaire . '_' . 'errorInput'] ) && in_array($name, $_SESSION[$this->nomFormulaire . '_' . 'errorInput'] ) ) echo ' class="erreur"';
          echo '>';
            if ( $label ) {
              echo '<label for="' . $name . '" ';
              echo ( $obligatoire ) ? ' class="obligatoire"': '';
              echo '>' . $label;
              echo ( $obligatoire ) ? ' <span class="alerte">*</span>': '';
              echo '</label>';
            }
            echo '<textarea name="' . $name . '" id="' . $name . '" ';
            echo ( $css ) ? 'class="' . $css . '" ' : '';
            echo '>';
            if ( isset ( $_SESSION[$this->nomFormulaire . '_' . $name] ) ) echo $_SESSION[$this->nomFormulaire . '_' . $name];
            echo '</textarea>';
          echo '</p>
  ';
          break;
        case "submit" :
          echo '<p>';
            echo '<input name="' . $name . '" id="' . $name . '" ';
            echo ( $css ) ? 'class="' . $css . '" ' : '';
            echo 'value="' . $label . '" type="submit">';
          echo '</p>
  ';
          break;
        case "tel" :
        case "email" :
        case "text" :
          echo '<p';
          if ( isset( $_SESSION[$this->nomFormulaire . '_' . 'errorInput'] ) && in_array($name, $_SESSION[$this->nomFormulaire . '_' . 'errorInput'] ) ) echo ' class="erreur"';
          echo '>';
            if ( $label ) {
              echo '<label for="' . $name . '" ';
              echo ( $obligatoire ) ? ' class="obligatoire"': '';
              echo '>' . $label;
              echo ( $obligatoire ) ? ' <span class="alerte">*</span>': '';
              echo '</label>';
            }
            echo '<input name="' . $name . '" id="' . $name . '" ';
            echo ( $css ) ? 'class="' . $css . '"" ' : '';
            echo 'value="';
            if ( isset ( $_SESSION[$this->nomFormulaire . '_' . $name] ) ) echo $_SESSION[$this->nomFormulaire . '_' . $name];
            echo '" type="text">';
          echo '</p>
  ';
          break;
        default :
          break;
      }
    }
    echo ( $obligatoires && isset($this->config['Traductions']['champs_obligatoires']) && $this->config['Traductions']['champs_obligatoires']!='' ) ? '<p class="alerte"><strong>* ' . $this->config['Traductions']['champs_obligatoires'] . ' </strong></p>
    ' : '' ;
    echo '</form>
  ';
  }
}
?>
