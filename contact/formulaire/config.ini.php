; <?php /*

; Formulaire contact exemple
; Dernière modification 17 mai 2016 par reaccess
; Toute ligne qui commence par un point virgule est un commentaire



[Champs]
; Liste formatée des champs
; *name* = *label*; *obligatoire* (defaut=non, oui/non); *type* (defaut=text, text/tel/email/textarea/submit); *css class*;
nom=Nom;oui
prenom=Prénom;oui
pays=Pays;oui
societe=Société;oui
telephone=Téléphone;oui;tel
email=Email;oui;email
message=Message;oui;textarea;textarea
action=OK;non;submit;bt_envoyer



[Email]
; Adresses mails des destiantaires du formulaire, séparées par des points virgules
destinataires=contact@natural-net.fr

; Defaut=text, choix : [vide]/text/html
format_mail=html

; Nom du fichier html de template du mail, defaut=[vide]
template_html=template_exemple.html



[Copie]
; Defaut=1, choix : [vide]/0/1
copie_internaute=1

; Defaut=email, nom du champ "email du demandeur" du formulaire
nom_champ_email=email



[Archivage]
; Defaut=1, choix : [vide]/0/1
logs=1



[Traductions]
; Sujet de l’email envoyé
mail_subject=formulaire contact Plagassol

; Texte situé en début de mail
mail_intro=Bonjour, une demande a été formulée sur le site Plagassol.

; Texte situé en fin de mail
mail_signature=Votre demande sera traitée dans les meilleurs délais. Merci de votre confiance. L’équipe de Plagassol.

; Mention "champs obligatoires"
champs_obligatoires=Champs obligatoires

; Titre du bloc "erreur de saisie" affiché en cas d’erreur de contrôle de saisie
erreur_saisie_titre=Erreur de saisie

; Texte en gras affiché en cas d’erreur de contrôle de saisie
erreur_saisie_soustitre=Le formulaire n’a pas été envoyé.

; Texte affiché en cas d’erreur de contrôle de saisie
erreur_saisie_texte=Certains champs obligatoires du formulaire sont non remplis ou mal remplis, merci de vérifier et de corriger.

; Titre du bloc "erreur de saisie" affiché en cas d’erreur de contrôle de saisie
erreur_technique_titre=Erreur technique

; Texte en gras affiché en cas d’erreur de contrôle de saisie
erreur_technique_texte=Une erreur technique est survenue. Le formulaire n’a pas été envoyé. Vous pouvez réessayer dans quelques instants. Si le problème persiste, merci de prendre contact avec le responsable du site.

; Titre du bloc de réussite d’envoi du formulaire affiché en cas de succès
succes_titre=Votre message a été envoyé

; Texte du bloc de réussite d’envoi du formulaire affiché en cas de succès
succes_texte=Votre demande sera traitée dans les meilleurs délais. Merci de votre confiance.



[Configuration]
smtp_from_email=contact@natural-net.fr
smtp_from_name=formulaire contact Plagassol
smtp_reply_email=
smtp_reply_name=



[Securite]
nom_de_domaine=cluster014.ovh.net
